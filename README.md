<table align="center"><tr><td align="center" width="9999">
<img src="img/dave.jpg" align="center" width="200" alt="Project icon">

# Zwift-Workouts

A project to collect my publicly available workouts created for Zwift users.
</td></tr></table>

All my public workouts are dynamic based on the rider's own unique FTP value. Please conduct a FTP or Ramp Test and set your FTP value in Zwift to gain the maximum advantage. Details on how to take a FTP test can be found **[here](https://zwift.com/news/4112-zwift-how-to-take-an-ftp-test?__znl=en-gb)**

If you wish to modify any files via code, the **[Zwift Workout File Reference can be found here](https://github.com/h4l/zwift-workout-file-reference/blob/master/zwift_workout_file_tag_reference.md)**

Workout files are free to use and modify. If you wish to use them, please follow the advice from Zwift Insider **[here](https://zwiftinsider.com/load-custom-workouts/)**

If you would like to create your own workouts for Zwift, or visualise/modify the ones that I provide, hop over to **[zwoFactory](http://zwofactory.com/)**

## Quick Start

1. **On a windows PC, locate your workouts directory.**

   This will be under 'Documents\Zwift\Workouts\{Your Account Number}

1. **Select a file from my list with a double click**

    Hit the download button and save it to your workouts directory

1. **Now open Zwift on your PC and view workouts**

   Your downloaded workout will be in the Custom Workouts section

1. **You can now close Zwift on your PC**

   The workout will be saved and available to any device you use to log into Zwift, including iOS and AppleTV

## Workouts

<table align="center">
<tr><th>Workout Name</th><th>Duration</th><th>Training Stress</th><th>Visualisation</th></tr>
<tr><td>Climbing Repeats</td><td>1:05:00</td><td>63</td><td><img src="img/ClimbingReps.JPG" align="center" width="400" alt="ClimbingReps"></td></tr>
<tr><td>Endurance Threshold</td><td>1:37:40</td><td>121</td><td><img src="img/EnduranceThreshold.JPG" align="center" width="400" alt="Endurance_Threshold"></td></tr>
<tr><td>Line Sprints</td><td>1:00:00</td><td>85</td><td><img src="img/LineSprints.JPG" align="center" width="400" alt="LineSprints"></td></tr>
<tr><td>Mornings</td><td>1:05:00</td><td>49</td><td><img src="img/Mornings.JPG" align="center" width="400" alt="Mornings"></td></tr>
<tr><td>Muscle Fibre Conditioning</td><td>1:05:00</td><td>89</td><td><img src="img/MuscleFibre.JPG" align="center" width="400" alt="Muscle_Fibre_Workout"></td></tr>
<tr><td>Over and Unders</td><td>1:13:30</td><td>97</td><td><img src="img/OverUnders.JPG" align="center" width="400" alt="Over_Under_Intervals"></td></tr>
<tr><td>Russian Steps (1 set)</td><td>0:39:00</td><td>42</td><td><img src="img/RussiansShort.JPG" align="center" width="400" alt="Russian Steps"></td></tr>
<tr><td>Russian Steps (2 sets)</td><td>1:22:00</td><td>107</td><td><img src="img/RussiansLong.JPG" align="center" width="400" alt="Russian-Steps-Long"></td></tr>
</table>


## Good luck!
## Dave

